# R Project

[![RStudio Version](https://img.shields.io/badge/RStudio-1.3-blue)](https://rstudio.com/)

End of course project, done by:

    - Estefani Paredes Valera

## 1. A description of each directory in the source code

```bash
RNSEQ/
    data/
    results/
    scriptsR/
        r1_bio_human_gene_listing.R
        r1_class1_history.R
        r1_help_paste.R
        r1_load_data.R
        r2_seq.R
        r2_vectors.R
        r3_statistics.R
        r4_graphics.R
        r5_searchabsL.R
        r6_gene_atomization.R
        r7_matrix.R
        r8_iris.R
        r10_curve.R
        r11_delim_sampleInfo.R
        r12_apply.R
        r12_equilibrio_harry_weinberg.R
        r13_rna_csv.R
        r14_practicaCOVID_estefani.R

```
These files are:
-   **RNSEQ/data/** The csv and txt files that I have used to make the queries are stored in this folder. 
-   **RNSEQ/results/** Images or PDF's are saved in this folder.
-   **RNSEQ/scriptsR/** Scripts are stored in this folder
-   **RNSEQ/scriptsR/r1_bio_human_gene_listing.R** In this file you can see: install pubmed.mineR , use word_atomizations and gene_atomization. [File](RNSEQ/scriptsR/r1_bio_human_gene_listing.R)
-   **RNSEQ/scriptsR/r1_class1_history.R** In this file you can see: help, mean, install packages and vectors. [File](RNSEQ/scriptsR/r1_class1_history.R)
-   **RNSEQ/scriptsR/r1_help_paste.R** In this file you can see: max, min, sample, factor, runif and paste[File](RNSEQ/scriptsR/r1_help_paste.R)
-   **RNSEQ/scriptsR/r1_load_data.R** In this file you can see: read.delim, table, barplot, colnames, apply.. [File](RNSEQ/scriptsR/r1_load_data.R)
-   **RNSEQ/scriptsR/r2_seq.R** In this file you can see: packages "gapminder", read.csv, seq, rep, sample, function... [File](RNSEQ/scriptsR/r2_seq.R)
-   **RNSEQ/scriptsR/r2_vectors.R** In this file you can see: scan, vector, matrix, rep, rbind, array, list...  [File](RNSEQ/scriptsR/r2_vectors.R)
-   **RNSEQ/scriptsR/r3_statistics.R** In this file you can see: database iris, var, runif, seq, quantile, rev, sort, factor...[File](RNSEQ/scriptsR/r3_statistics.R)
-   **RNSEQ/scriptsR/r4_graphics.R** In this file you can see: sample, factor, rep, data.frame... [File](RNSEQ/scriptsR/r4_graphics.R)
-   **RNSEQ/scriptsR/r5_searchabsL.R** In this file you can see: readabs, searchabsL and word_atomizations  [File](RNSEQ/scriptsR/r5_searchabsL.R)
-   **RNSEQ/scriptsR/r6_gene_atomization.R** In this file you can see: readabs, word_atomizations, searchabsL, rainbow, data.frame, apply, sapply, median...[File](RNSEQ/scriptsR/r6_gene_atomization.R)
-   **RNSEQ/scriptsR/r7_matrix.R** In this file you can see: sample, factor, matrix, data.frame... [File](RNSEQ/scriptsR/r7_matrix.R)
-   **RNSEQ/scriptsR/r8_iris.R** In this file you can see: iris, plot, hist, boxplot, points, legend, lines, abline, axis, text... [File](RNSEQ/scriptsR/r8_iris.R)
-   **RNSEQ/scriptsR/r10_curve.R** In this file you can see: for, matrix, curve, abline, read.csv ...[File](RNSEQ/scriptsR/r10_curve.R)
-   **RNSEQ/scriptsR/r11_delim_sampleInfo.R** In this file you can see: read.delim, boxplot, hist, summary, barplot... [File](RNSEQ/scriptsR/r11_delim_sampleInfo.R)
-   **RNSEQ/scriptsR/r12_apply.R** In this file you can see: seed, seq, data.frame, cov, points... [File](RNSEQ/scriptsR/r12_apply.R)
-   **RNSEQ/scriptsR/r12_equilibrio_harry_weinberg.R** In this file you can see: practice of [harry-weinberg balance](https://es.wikipedia.org/wiki/Ley_de_Hardy-Weinberg#:~:text=En%20el%20lenguaje%20de%20la,un%20valor%20de%20equilibrio%20particular.)[File](RNSEQ/scriptsR/r12_equilibrio_harry_weinberg.R)
-   **RNSEQ/scriptsR/r13_rna_csv.R** In this file you can see: dir, setwd, list.files, read... [File](RNSEQ/scriptsR/r13_rna_csv.R)
-   **RNSEQ/scriptsR/r14_practicaCOVID_estefani.R** In this file you can see: you can see my COVID practice. [File](RNSEQ/scriptsR/r14_practicaCOVID_estefani.R)


## 2. Running the Project Locally.

The essential first step is to [install RStudio](https://linuxconfig.org/rstudio-on-ubuntu-18-04-bionic-beaver-linux). We work in Ubuntu 18.04, the commands are:

```bash
sudo apt update
sudo apt -y install r-base
sudo apt install gdebi-core
```
Next, navigate your browser to the official [RStudio download page](https://rstudio.com/products/rstudio/download/#download) and download the latest Ubuntu/Debian RStudio *.deb package available.

```bash
cd Downloads/
ls
```

```bash
sudo gdebi rstudio-xenial-1.1.442-amd64.deb
rstudio 
```
>*rstudio : with this command start the RStudio*

>*Information extracted from this [website](https://linuxconfig.org/rstudio-on-ubuntu-18-04-bionic-beaver-linux)*


Now, clone the repository to your local machine:

```bash
git clone https://gitlab.com/estefani.val34/r.git
cd r/
```

Call the r / folder in the script that we are going to execute.

```bash
setwd("/home/user/r")
```
> *If you get any errors, check that you are in the correct folder or file (path) and check that you have downloaded the necessary packages.*









