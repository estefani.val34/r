getwd()
help.start()
help("mean")
0:10
x <- 0:10
x
xm <- mean(x)
xm
?mean
help.search("mean")
apropos("mean")
apropos("mean", mode="function")
data()
CO2
airquality
head(airquality)
head(iris)
help(iris)
getwd()
setwd("ruta")
ls()
dir()
history()
savehistory()

rm(x)
x <- 1:10
x
mean(x)

rm(x)
x = 1:10
x
mean(x)

# caso 1

rm(x) 
x
mean(x = 1:10)
x

# caso 2

a<-2
a
class(a)

a <-"hola"

b <- "adios"
class(b)


a <-2
c <- a+b


2/3
2/0
0/0
2i2
3+NA
3+ad
3*Inf
Inf-Inf


2^3
2%%3
2%/%3


sqrt(2)
pi
log10(1000)
sin(pi/2)
log(-5)
log(0)
c=3
getOption("digits")
old <- getOption("digits")
getOption(digits=15)

2/3

?rnorm
x <- rnorm(500, 165,20)
x
?hist
hist(x)

source("histogram.R")

.libPaths()
installed.packages()  
search() 

packageDescription("stats")
help(package="stats")

packageDescription("vioplot")

install.packages("vioplot")
install.packages("tcltk")
library(vioplot)



x <- 5
print(x)
class(x)

x <- 5L
print(x)
class(x)

x <-2+3i
print(x)
class(x)

a <- "hola"
class(a)

s <- TRUE
class(s)


vector1 <- c(1,2,3)
vector2 <- 2*vector1
vector1[3]
v1 <- vector("integer",2)
v1
v1 <- vector("logical",2)
v1 <- 100:1
v1



u <- c(1,2,3 ) 
v <- c(2,4,6)
w <- c(u,v)
