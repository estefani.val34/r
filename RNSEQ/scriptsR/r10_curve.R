#estrudcturas de control


# if
#if(condition){
#}else{
#}

#dado un numero ente 1 y 100 , decidir si un numeros valido de cromosoma de 1 a 22

x<- 1:100

if(sample(x, 1,replace = TRUE )<=22){
  print( "el numero se corresponde con un cromosoma")
}else{
  print("el numero no se corresponde con el cromosoma")
}

# if anindados

nota <- 9

if(nota<5 ){
  print("suspenso")
}else if (nota>=5 && nota <6){
  print("suficiente")
}else if (nota>6 && nota <=8){
  print("Notable")
}else if (nota>=9 && nota<=10){
  print("excelente")
}else{
  print("nota fuera de rango")
}

# for

for (i in 1:10) {
  print(i)
}

x <-c(1,8,3,4)
for (i in x) {
  print(x[i])
}

y <- c("a","b","c")
for (letra in y) { #TOMA LA LETRA COMO una variable , falla 
  print(y[letra])
}

for (letra in c("a","b","c")) {
  print(letra)
}

y <- c("a","b","c")
for (letra in y) { 
  print(letra) # for each, ya te da el elemento 
}

letters
LETTERS

# para el funcionamiento con el indice 
y <- c("a","b","c")
for (i in 1:3) { 
  print(y[i]) 
}

# imprimir las letras de un for 
for (letter in letters) {
  print(letter)
}

letters[4]
# imprimir en orden descendente , inverso
for (i in length(letters):1) {
  print(letters[i])
}

for (i in 1:10) {
  print(paste("gen",i,sep = ""))
}
# loops aplicados a matrices

matriz<- matrix(1:12,4,3)# lo rellena por columnas
matriz
for (i in matriz) {
  print(i)
}

seq_along(matriz) # indices
for (i in seq_along(matriz)) {
  print(matriz[i])
}

seq_len(nrow(matriz))
seq_len(ncol(matriz)) # da el numero de columnas
for (i in seq_len(ncol(matriz))) {
  print(matriz[i])
}

# for anidado 
my_matriz<- matrix(1:6,2,3, by=row)
? matrix
for (row in seq_len(nrow(my_matriz))) { # filas
  for (col in seq_len(ncol(my_matriz))) { # columnas
    print(my_matriz[row,col])
  }
}

for (row in 1:nrow(my_matriz)) { # filas
  for (col in 1:ncol(my_matriz)) { # columnas
    print(my_matriz[row,col])
  }
}


# while : comprueva esa condicion , has que se cumple la condcion 
# imprimir los 10 primeros numeros 
# + : escape 
count<-0
while(count<10){
  print(count)
  count<- count+1
}

# imprimir nuemros del 34-39
z<-34
while(z>=34 && z<=39){
  print(z)
  z<-z+1
}

###################################
# rnorm , la normal 
?par # definir el espacio grafico 
pdf("hist.pdf")
hist(rnorm(400)) # genera 400 numeros y ahcemos un histograma con ellos 
dev.off() # cierra el archivo
rnorm(400)
getwd()


# la fucnion curve hace graficas de una fucnion continua sobre un ragno de valores
# df son los grados de libertad, la forma de la grafica sdepende de los grados de libertad
curve(dchisq(x,df=1), col="red", xlim =c(0,20) , ylim=c(0,0.2), lwd=2, main="chicuadrado", xlab = "x", ylab = "densidad ")

curve(dchisq(x,df=2), col="orange", lwd=2, lty=7, add = T)
curve(dchisq(x,df=4), col="purple", lwd=2, lty=7, add = T)
# alginas graficas de alto nuveles te permiten sobreponer una imagen 
curve(dchisq(x,df=10), col="pink", lwd=2, lty=7, add = T)
curve(dchisq(x,df=20), col="yellow", lwd=2, lty=7, add = T)

abline(h=0, col="gray")
abline(v=0, col="gray")

rnorm(400)

dev.off() 
m<-seq(700, 1000, by=100)
par(mfrow=c(2,2))
n<- 1:4

for (i in n) {
    x<- rnorm(m[i])
    hist(x,main = paste("histograma",i),xlab = paste("Normal", m[i]))
}

#Ejercicio 2
#investigar islas  CpG sites , C:citosona, p:fosfor, G:guanina: regiones donde las partes promotoras estan formadas por estas islas. 
#genes constitutivos : lo que tienen todas las celulas 
#genes que se expresan en tejidos especificos: genes especificos en una celula
#
#de los genes 
#

#ejercicio
#investigar si hay una relacion (correlacion lineal) entre los niveles de expresion y las islas CpG 
#1.- seleccionar la expresion de los 500 primeros genes

expresion<- read.csv("data/exp_genes_mouse.csv")
str(expresion)
metadatos<- read.csv("data/metadata_mouse.csv")
str(metadatos)

#2.-escribir en un archivo los nombres de los primeros 500 genes por biomart 
expresion_500<- head(expresion, 500) # ENS (ensembol)MUSG00000000435(id del ensembol)
# sacar el nombre de las filas para poder ponerlo en ensembol
genes_500.csv<- row.names(expresion_500) # rownames
write.csv(genes_500.csv,row.names = FALSE,quote = FALSE, file = "data/genes_500.csv")# QUE ME LO DEJE POR COLUMNAS , quitar las comillas
# sube el archivo al ensenble








